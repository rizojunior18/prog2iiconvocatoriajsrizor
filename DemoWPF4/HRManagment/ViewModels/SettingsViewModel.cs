﻿using System.Configuration;

namespace HRManagment.ViewModels
{
    public class SettingsViewModel
    {
        public string Title { get; set; }
        public string Directory { get; set; }
        public SettingsViewModel()
        {
            Title = ConfigurationManager.AppSettings.Get("Title");
            Directory = ConfigurationManager.AppSettings.Get("Directory");
        }
        public void SaveSettings()
        {
            Configuration settings = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (settings.AppSettings.Settings["Title"] == null)
            {
                settings.AppSettings.Settings.Add("Title", Title);
            }
            else
            {
                settings.AppSettings.Settings["Title"].Value = Title;
            }
            if (settings.AppSettings.Settings["Directory"] == null)
            {
                settings.AppSettings.Settings.Add("Directory", Directory);
            }
            else
            {
                settings.AppSettings.Settings["Directory"].Value = Directory;
            }
            
            settings.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }
    }
}

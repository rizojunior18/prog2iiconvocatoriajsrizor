﻿using HRManagment.Controllers;
using HRManagment.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HRManagment.Views.UserControls
{
    /// <summary>
    /// Interaction logic for SettingsDialog.xaml
    /// </summary>
    public partial class SettingsDialog : UserControl
    {
        SettingsController sc;
        public SettingsDialog()
        {
            InitializeComponent();
            SetupController();
        }
        public void SetupController()
        {
            sc = new SettingsController(this);
            RoutedEventHandler route = new RoutedEventHandler(sc.SettingsEventHandler);
            ChooseButton.Click += route;
            OkButton.Click += route;
            CancelButton.Click += route;
        }
        public void Hide()
        {
            Visibility = Visibility.Collapsed;
        }
        public void SetDirectory(string value)
        {
            SettingsViewModel vm = this.DataContext as SettingsViewModel;
            vm.Directory = value;
            DirectoryTextBox.Text = value;
        }
    }
}

﻿using Entities.Models;
using HRManagment.Views.UserControls;
using Microsoft.Win32;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace HRManagment.Controllers
{
    public class PersonController
    {
        PersonForm pwindow;
        SaveFileDialog sfdialog;
        OpenFileDialog ofdialog;
       
         public PersonController(PersonForm form)
        {
            pwindow = form;
            sfdialog = new SaveFileDialog();
            ofdialog = new OpenFileDialog();
        }
        public void PersonEventHandler(object sender, RoutedEventArgs e)
        {
            Button B = (Button)sender;
            switch (B.Name)
            {
                case "SaveButton":
                    SaveData();
                    break;
                case "OpenButton":
                    OpenFile();
                    break;
            }
        }

        private void OpenFile()
        {
            ofdialog.Filter = "Json File (*.json)|*.json";
            if (ofdialog.ShowDialog() == true)
            {
                Person p = new Person();
                pwindow.SetData(p.FromJson(ofdialog.FileName));               

            }
        }

        private void SaveData()
        {
            sfdialog.Filter = "Json File (*.json)|*.json";
            if (sfdialog.ShowDialog() == true)
            {
                Person p;
                
                    p = pwindow.GetData();

                p.ToJson(sfdialog.FileName);
            }
        }

        private void SaveCollection()
        {
            sfdialog.Filter = "Json File (*.json)|*.json";
            if (sfdialog.ShowDialog() == true)
            {
                Group g = new Group
                {
                    Name = "Grupo de demostración de ususarios",
                    Members = new List<Person>
                    {
                        new Person() { Names = "William", LastNames = "Sanchez", BirthDate = new System.DateTime(1983, 9, 1), State = "Masaya" }
                    }
                };

                Person p = new Person() { Names = "Douglas", LastNames = "Sanchez", BirthDate = new System.DateTime(1982, 08, 17), State = "Masaya" };
                g.Members.Add(p);

                g.Members.Add(new Person() { Names = "Erick", LastNames = "Sanchez", BirthDate = new System.DateTime(1984, 9, 1), State = "Masaya" });
                g.Members.Add(new Person() { Names = "Junior", LastNames = "Rizo", BirthDate = new System.DateTime(1985, 6, 8), State = "Masaya" });
                g.Members.Add(new Person() { Names = "Ricardo", LastNames = "Sanchez", BirthDate = new System.DateTime(1988, 5, 6), State = "Masaya" });

                g.ToJson(sfdialog.FileName);
            }
        }
    }
}

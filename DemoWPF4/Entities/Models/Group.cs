﻿using Entities.Interfaces;
using Lib.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
    public class Group:IWSFile<Group>
    {
        public string Name { get; set; }
        public int? Length { get { return Members?.Count; } }
        public ICollection<Person> Members { get; set; }

       public Group FromJson(string Path)
        {
            return JsonSerialization.ReadFromJsonFile<Group>(Path);
        }

        public void ToJson(string Path)
        {
            JsonSerialization.WriteToJsonFile(Path, this, false);
        }
    }
}
